# data-analytics

## Purpose of Project

Create a open public space, including data and data-science environments, for analyzing data about policing and prisons. 

## Data Collection and Preparation

The first area of focus for this project is the collection and preparation of data for analysis. Data on policing is difficult to get and will likely also require cleaning, augmenting and transformation to make it useful for analysis.

If you'd like to contribute new data please make a merge question with the data file added to `/data`

## Data Analysis

The second area of focus for this project is data analysis. In order to streamline and standardize this analysis for both transparency and decentralized and asynchronous work I have created two analysis environments managed by docker. 

Both of these environments can be stood up by cloning this repository and from this directory running

`docker-compose up -d`

To see the running containers run
`docker ps`

### jupyter

To access the jupyter environment run `docker logs {container name}` where the container name is found after running `docker ps`. It will probably be `data-analytics_jupyter_1`.

you should see a log that looks like this
```
[I 19:10:47.610 NotebookApp] Writing notebook server cookie secret to /home/jovyan/.local/share/jupyter/runtime/notebook_cookie_secret
[I 19:10:47.795 NotebookApp] Loading IPython parallel extension
[I 19:10:48.122 NotebookApp] JupyterLab extension loaded from /opt/conda/lib/python3.8/site-packages/jupyterlab
[I 19:10:48.122 NotebookApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
[I 19:10:48.124 NotebookApp] Serving notebooks from local directory: /home/jovyan
[I 19:10:48.124 NotebookApp] The Jupyter Notebook is running at:
[I 19:10:48.124 NotebookApp] http://82f5ef52c9b6:8888/?token=6e3550be2ee3418d3de85765efe809612c31086279ab551f
[I 19:10:48.124 NotebookApp]  or http://127.0.0.1:8888/?token=6e3550be2ee3418d3de85765efe809612c31086279ab551f
[I 19:10:48.124 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 19:10:48.127 NotebookApp]

    To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-7-open.html
    Or copy and paste one of these URLs:
        http://82f5ef52c9b6:8888/?token=6e3550be2ee3418d3de85765efe809612c31086279ab551f
     or http://127.0.0.1:8888/?token=6e3550be2ee3418d3de85765efe809612c31086279ab551f
```
The important part being the full url, token included at the end. 

You should be able to visit that URL and access the jupyter environment. Some brief tutorials that demonstrate how to access the data will be found in the `/work folder

### zeppelin  

Once you've run `docker-compose up -d` you should be able to access zeppelin at http://localhost:8080/

There you should be able to navigate to a tutorial on how to connect to our data sources. 